import { ListStudentComponent } from './list-student/list-student.component';

import { LoginComponent } from './login/login.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { FromstudentComponent } from './fromstudent/fromstudent.component';





const routes: Routes = [



  { path: 'signup', component: SignupComponent},
  { path: 'login', component: LoginComponent},
  { path: 'welcome', component: WelcomeComponent},
  { path: 'fromstudent', component: FromstudentComponent},
  { path: 'students', component: ListStudentComponent},

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }