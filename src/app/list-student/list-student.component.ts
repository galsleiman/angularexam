import { AuthService } from './../auth.service';
import { StudentService } from './../service/student.service';
import { Component, OnInit } from '@angular/core';
import { Student } from '../interface/student';

@Component({
  selector: 'app-list-student',
  templateUrl: './list-student.component.html',
  styleUrls: ['./list-student.component.css']
})
export class ListStudentComponent implements OnInit {

  addTaskFormOpen = false;
  panelOpenState = false;
students$;
students:Student[];
userId:string;

  
delete(id:string){
  console.log(id)
  this.studentService.deletea(this.userId,id);
}
  

  

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
 

        this.students$ = this.studentService.getAll(this.userId);

        this.students$.subscribe(
          docs =>{
            this.students = [];
            for(let document of docs){
              const student:Student = document.payload.doc.data();
              student.id = document.payload.doc.id; 
              this.students.push(student); 
            }
          }
        ) 


      }
    )

     


  }

 constructor(private studentService:StudentService,public authService:AuthService) { }
}