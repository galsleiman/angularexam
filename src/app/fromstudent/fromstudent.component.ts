import { AuthService } from './../auth.service';
import { StudentService } from './../service/student.service';
import { Student } from './../interface/student';
import { EventEmitter } from '@angular/core';
import { Output } from '@angular/core';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'fromstudent',
  templateUrl: './fromstudent.component.html',
  styleUrls: ['./fromstudent.component.css']
})
export class FromstudentComponent implements OnInit {

  types:Object[] = [true,false]
  type2s:Object[] = ["Will drop out", "complete the degree"]
  pre:false;
  predict:string="";
userId:string;
email:string; 
user$;

  @Input() math:number;  
  @Input() psy:number; 
  @Input() payall:boolean;
  @Input() id:string;
  @Output() update = new EventEmitter<Student>();
  @Output() closeEdit = new EventEmitter<null>();
students:Student[];

  updateParent(){

    let student:Student = {id:this.id, math:this.math, psy:this.psy,payall:this.payall};
    this.update.emit(student); 
 
   this.studentService.addstudent(this.userId, this.id ,this.math, this.psy,this.payall,this.predict,this.email); 

    }
  

classify(){
  console.log( this.math, this.psy,this.payall)
  this.studentService.classify( this.id ,this.math, this.psy,this.payall).subscribe(
    res => {
     console.log(res);
   
        if (res>0.5){
          res='complete the degree'
        }else{ res ='Will drop out'}
        this.predict=res
 
      
    }
  )

}


  tellParentToClose(){

    this.math=null;
    this.psy=null;
    this.payall=null;
    this.pre=false;
    this.predict="";
  //this.closeEdit.emit(); 
  }


  constructor(private studentService:StudentService,public authService:AuthService) { }

 

ngOnInit(): void {

  this.authService.getUser().subscribe(
    user => {
      this.userId = user.uid;
    this.email=user.email;

         }
      )
}}