import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FromstudentComponent } from './fromstudent.component';

describe('FromstudentComponent', () => {
  let component: FromstudentComponent;
  let fixture: ComponentFixture<FromstudentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FromstudentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FromstudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
