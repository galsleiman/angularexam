import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  private url =  " https://6mu4cu8635.execute-api.us-east-1.amazonaws.com/beta";
  
  studentCollection:AngularFirestoreCollection;  
  userCollection:AngularFirestoreCollection = this.db.collection('users');
emailsave:string;
  

  classify(id:string,math:number,psy:number,payall:boolean){
    let json = {'data':
      {'math':math,
      'psy':psy,
      'payall':payall
    }
    }
    console.log(json);
    let body = JSON.stringify(json);
    console.log(body)
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
      console.log(res)
      let final = res.body;
      console.log(final)

      return final
  
    })
    )
        
  
  }
  
  addstudent(userId:string,id:string,math:number,psy:number,payall:boolean,predict:string,email:string){
    let date:any = new Date()
    date = Number(date);
     const student = { 
       math:math,
      psy:psy,
      payall:payall,
      predict:predict,
      emailall:email,
      date:date
    }; 
    console.log(math,psy,payall,predict,userId)
    this.userCollection.doc(userId).collection('students').add(student);
    this.router.navigate(['/students']); 

  }
  
  
  deletea(Userid:string, id:string){
    this.db.doc(`users/${Userid}/students/${id}`).delete(); 
  } 

 

  public getAll(userId){
    this.studentCollection = this.db.collection(`users/${userId}/students`,ref =>ref.orderBy('date','desc'));
    return this.studentCollection.snapshotChanges()
     
  }
  
    constructor(private http:HttpClient,private db:AngularFirestore,private router:Router) { }
  }
  