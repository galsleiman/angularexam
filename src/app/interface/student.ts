export interface Student {
    id:string,
    math:number,
    psy:number,
    payall?:boolean,
    predict?:string;
    saved?:Boolean;
    emailsave?:string;
    time?:number;
}
