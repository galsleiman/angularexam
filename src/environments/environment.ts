// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig :{
    apiKey: "AIzaSyDUD9L7D5rNvAI47F-wijeH-l0fyEve_ko",
    authDomain: "angularexamgal.firebaseapp.com",
    projectId: "angularexamgal",
    storageBucket: "angularexamgal.appspot.com",
    messagingSenderId: "949914246490",
    appId: "1:949914246490:web:f7c46e7b107fc955432763"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
